
from .ai import *
import csv


if __name__ == '__main__':
    N = 10
    generations = 1
    pool = multiprocessing.Pool(processes=7)
    # Create initial state
    initial_state = init_del_loma_smol()
    initial_state[7, 150, 150] = 1
    available_resources = {
        'bulldozer': 30
    }
    # Come up with intervention plans
    population = [[
        BullDozedFireLine(
            (130 + np.random.randint(50), 150 - np.random.randint(30) - 15),
            (180 + np.random.randint(30) - 15, 150 - np.random.randint(30) )
        ),
    ] for i in range(N)]
    #slope of good fire line =
    # THE CIRCLE OF LIFE
    for generation in range(generations):
        # Test intervention plans
        final_states = [
            pool.map(simulation_worker,
                     [
                         (
                             deepcopy(initial_state),
                             deepcopy(available_resources),
                             deepcopy(plan),
                             30, False
                         )
                         for i in range(7)
                     ]
                     )
            for plan in population
        ]
        # Evaluate the final states
        final_scores = [(i, np.mean([fitness(state) for state in states]))
                        for i, states in enumerate(final_states)]
        # Generate rankings
        final_score_rankings = sorted(
            final_scores,
            key=lambda x: x[1],
            reverse=True
        )
        # Cull the bottom 50% and discard the fitnesses
        survivors = final_score_rankings[:int(len(final_score_rankings) / 2)]
        survivors = [population[i] for i, fitness in survivors]
        # Make the fittest breed
        new_population = []
        for i in range(N):
            a, b = np.random.choice(len(survivors), 2)
            new_individual = frick(survivors[a], survivors[b])
            # Randomly mutate
            if np.random.random() < 0.5:
                new_individual = mutate(new_individual)
            new_population.append(new_individual)
        population = new_population

    # Test intervention plans
    simulation = SimulationManager()
    final_states = [
        pool.map(simulation_worker,
                    [
                        (
                            deepcopy(initial_state),
                            deepcopy(available_resources),
                            deepcopy(plan),
                            30, False
                        )
                        for i in range(7)
                    ]
                    )
        for plan in population
    ]
    # Evaluate the final states
    final_scores = [(i, np.mean([fitness(state) for state in states]))
                    for i, states in enumerate(final_states)]
    # Generate rankings
    final_score_rankings = sorted(
        final_scores,
        key=lambda x: x[1],
        reverse=True
    )
    
    with open('del_loma_ds.csv', 'w') as f:
        writer = csv.write(f)
        for score in final_score_rankings:
            writer.writerow(initial_state)
            writer.writerow(score)


   

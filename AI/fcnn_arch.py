import torch
from torch.nn.modules.activation import Sigmoid, Tanh
from torch.nn.modules.conv import ConvTranspose2d
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.functional as F
import numpy as np
import random


# Define a Dataset class that contains your training data
class MyTrainingDataset(Dataset):
    def __init__(self):
        # Load and preprocess your training data here
        self.data = ...
        self.labels = ...

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        return self.data[index], self.labels[index]


# Create a DataLoader object to iterate over the training data in mini-batches
train_dataset = MyTrainingDataset()
train_dataloader = DataLoader(train_dataset, batch_size=32, shuffle=True)

# Define the FCNN model
class FCNN(nn.Module):
    def __init__(self):
        super(FCNN, self).__init__()

        # Creating convolutional layers
        self.fcnn_model = nn.Sequential(
            nn.Conv2d(in_channels=9, out_channels=64, kernel_size=5),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2, stride=5),
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=5),
            nn.Tanh(),
            nn.AvgPool2d(kernel_size=2, stride=5),
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=5),
            nn.Tanh(),
            nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=5, stride=2),
            nn.Tanh(),
            nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=4),
            nn.Tanh(),
            nn.ConvTranspose2d(in_channels=64, out_channels=1, kernel_size=4, stride=4),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.fcnn_model(x)
        return x

# Choose a loss function and an optimizer
fcnn = FCNN()
loss_fn = nn.MSELoss()
optimizer = torch.optim.Adam(fcnn.parameters(), lr=1e-3)

# Training loop
for epoch in range(10):  # number of training epochs
    for i, (data, labels) in enumerate(train_dataloader):
        # Clear the gradients
        optimizer.zero_grad()

        # Forward pass
        output = fcnn(data)

        # Compute the loss
        loss = loss_fn(output, labels)

        # Backpropagate the error
        loss.backward()

        # Update the model parameters
        optimizer.step()

        # Print the loss
        print(f"Epoch {epoch}, loss: {loss.item()}")

# Save the trained model
torch.save(fcnn.state_dict(), "fcnn_model.pt")
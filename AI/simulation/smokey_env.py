import gym
import numpy as np
from grid_init import init_del_loma

#For some unknown and idiotic reason, gym env uses four identations instead of two - WATCH OUT FOR THAT

class FireEnv(gym.Env):
  def __init__(self):
        super(FireEnv, self).__init__()
        # Initialize variables and set the initial state of the environment
        pass

  def reset(self):
        grid = init_del_loma()
        self.done = False
        
        self.observation = grid

        return self.observation
    

  def step(self, action):
        # Update the environment's state based on the given action
        # Return a tuple (observation, reward, done, info)
        ny, nx = self.observation.shape[1:]
        # Current state
        wind_speed = self.observation[1, :, :]
        wind_direction = self.observation[2, :, :]
        fuel = self.observation[3, :, :]
        humidity = self.observation[4, :, :]
        precipitation = self.observation[5, :, :]
        altitude = self.observation[6, :, :]
        burning = self.observation[7, :, :]

        # Probability that there will be a fire
        prob = np.zeros((ny, nx))
        # Probability multipliers (initialised to -1)
        prob_multipliers = np.ones((ny, nx)) * -1
        for y in range(1, ny - 1):
            for x in range(1, nx - 1):
                neighbourhood = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1),
                                (1, 0), (1, 1)]
                if burning[y, x] == 1:
                    # If we are already burning...
                    # Simulate the impact of embers flying off and starting more fires far away
                    if np.random.random() < 0.0002:
                        # The distance the ember travels
                        distance = int(wind_speed[y, x] * 1.5)
                        # Calculate the displacement assuming the ember travels with the wind
                        wind_direction_rad = np.radians(wind_direction[y, x])
                        dy = int(distance * - np.cos(wind_direction_rad))
                        dx = int(distance * np.sin(wind_direction_rad))
                        # Stay within grid bounds
                        if y + dy in range(ny) and x + dx in range(nx):
                            # Let the ember land on the ground and potentially start a fire.
                            prob_multipliers[y + dy, x + dx] = 2
                else:
                    # If we are not burning, check if we should burn
                    for neighbour in neighbourhood:
                        dx, dy = neighbour
                        neighbour_on_fire = burning[y + dy, x + dx]
                        if neighbour_on_fire == 1:
                            if prob_multipliers[y, x] == -1:
                                prob_multipliers[y, x] = 1
                            # Slightly lower probability increase if the burning neighbour is diagnonal
                            prob_multipliers[y, x] *= 1.2 if np.abs(dy) + np.abs(
                                dx) < 2 else 1.14

                            # Wind impact
                            prob_multipliers[y, x] *= 1 + np.dot(
                                [-dy, -dx]/np.linalg.norm(neighbour),
                                [
                                    -np.cos(np.radians(wind_direction[y + dy, x + dx])),
                                    np.sin(np.radians(
                                        wind_direction[y + dy, x + dx]))
                                ]
                            )
                            prob_multipliers[y, x] *= (1 + wind_speed[y, x])/20

                            # Slope
                            m = (altitude[y, x] - altitude[y+dy, x+dx]) / \
                                (np.abs(np.linalg.norm(neighbour)) * 10)
                            prob_multipliers[y, x] *= (1+m) if m > 0 else (1/(1-m))

        # Precipitation
        burning -= precipitation / 30 # Assume 30mm of rain will kill the fire
        # Humidity
        prob_multipliers *= 1 / (1 + 0.007 * humidity)
        # Compute the probability of burning
        prob = np.maximum(
            np.clip(np.multiply(fuel, prob_multipliers), 0, 1),
            np.clip(burning, 0, 1)
        )
        # Compute the new state by sampling probabilities at each cell
        new_burning = np.zeros((ny, nx))
        random_matrix = np.random.random((ny, nx))
        new_burning = (random_matrix < prob).astype(float)
        self.observation[7, :, :] = new_burning
        # Compute the reward
        #TODO: Change reward to increase according to what is burning, i.e. houses, forests, etc.
        #Minus one for penalisng the agent for each cell that is burning
        self.reward = np.sum(new_burning)*-1

        info = {}
        return self.observation, self.reward, self.done, info

  def render(self):
        # Return a representation of the environment's current state
        pass